import { Component, OnInit } from '@angular/core';
import { PresenceService } from '../services/presence.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'app-user-status',
  templateUrl: './user-status.component.html',
  styleUrls: ['./user-status.component.scss']
})
export class UserStatusComponent implements OnInit {

  constructor(
    public presenceService: PresenceService,
    public afAuth: AngularFireAuth
  ) { }

  ngOnInit() {
  }

  
  login() {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }

}
