import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CaptureScreenService {

  constructor() { }

  capture(): Promise<MediaStream>{
    let nav = <any>navigator;
    if (nav.getDisplayMedia) {
      return nav.getDisplayMedia({video: {width: {max: 1920}, height: {max: 1080}, frameRate: {max: 30}}});
    } else if (nav.mediaDevices.getDisplayMedia) {
      return nav.mediaDevices.getDisplayMedia({video: {width: {max: 1920}, height: {max: 1080}, frameRate: {max: 30}}});
    } else {
      return nav.mediaDevices.getUserMedia({video: {mediaSource: 'screen'}});
    }
  }
}
