import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import * as firebase from 'firebase/app';
import { tap, map, switchMap, first } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PresenceService {

    public sessionId: string;
    public userId: string;
    public displayName: string = 'Unknown';
    sessionEntry: any;

    constructor(
        private afAuth: AngularFireAuth, 
        private db: AngularFireDatabase,
    ) {
        this.sessionId = 'session-' + this.guid();
        this.afAuth.authState.subscribe( user => {
            if(user){
                this.userId = user.uid;
                if(user.displayName){
                    this.displayName = user.displayName;
                }
                this.sessionEntry = this.db.list('sessions').push({
                    sessionId: this.sessionId,
                    userId: this.userId
                });
                this.db.object(`sessions/${this.sessionEntry.key}`).query.ref.onDisconnect().remove();
            } else if (this.sessionEntry){
                this.sessionEntry.remove();
            }
        });
    }

    signOut() {
        this.sessionEntry.remove()
        .then( () => this.afAuth.auth.signOut());
    }

    
    guid() {
        return (this.s4() + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + this.s4() + this.s4());
    }
    
    s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

}