import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter, ElementRef, HostBinding } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
declare let RTCPeerConnection: any;

@Component({
  selector: 'app-peer-connection',
  templateUrl: './peer-connection.component.html',
  styleUrls: ['./peer-connection.component.scss']
})
export class PeerConnectionComponent implements OnInit, OnDestroy {

    @Input() peerSessionId: string;
    @Input() remoteClientId: string;
    @Input() localStreamSrc: any = null;
    @Input() sendOffer: boolean = false;

    @Output() connectionStateChange = new EventEmitter();

    @HostBinding('class.remote') remotePeerClass: boolean = false;

    remoteStreamSrc: MediaStream = new MediaStream();
    remoteStreamRunning: boolean = false;
    connection: string = 'disconnected';
    pc: any;
    senderId: string;
    signallingChannel: AngularFireList<any>;
    signallingChannelSubscription: Subscription;
  
    @ViewChild("remote") remoteVideo: ElementRef;
  
    constructor(
      private db: AngularFireDatabase
    ) {}
  
    ngOnInit() {
      this.setupWebRtc();
      this.signallingChannel = this.db.list('signaling/' + this.peerSessionId);
      this.signallingChannelSubscription = this.signallingChannel.stateChanges(['child_added'])
        .subscribe(action => {
          if(action.payload.val()){
            this.readMessage(action.payload.val())
          }
        });
      if(this.sendOffer){
        this.createOffer();
        this.remotePeerClass = true;
      }

      this.remoteStreamSrc.addEventListener('removetrack', (event) => {
        console.log(`${event.track.kind} track removed`);
      });
      this.remoteStreamSrc.addEventListener('inactive', (event) => {
        console.log(`track inactive`);
      })
    }
  
    ngOnDestroy() {
      console.log('destroyed component');
      this.hangup();
    }
  
    setupWebRtc() {
      this.senderId = this.guid();
      console.log('senderId: ' + this.senderId);
  
      try {
        this.pc = new RTCPeerConnection({
          iceServers: [
            { urls: "stun:stun.services.mozilla.com" },
            { urls: "stun:stun.l.google.com:19302" }
          ]
        }, { optional: [] });
      } catch (error) {
        console.log(error);
      }

      this.pc.onconnectionstatechange = event => {
        this.connectionStateChange.emit(this.pc.connectionState);
        this.connection = this.pc.connectionState;
      }
    
      this.pc.onicecandidate = event => {
        event.candidate ? this.sendMessage(this.senderId, JSON.stringify({ ice: event.candidate })) : console.log("Sent All Ice");
        console.log(event);
      }
  
      this.pc.ontrack = event => {
        console.log('ontrack event');
        console.log(event);
        this.remoteStreamSrc.addTrack(event.track);
        this.remoteStreamRunning = true;
        this.remoteVideo.nativeElement.srcObject = this.remoteStreamSrc;
        this.remoteVideo.nativeElement.srcObject.onremovetrack = event => {
          console.log('onremovetrack event')
        }
        this.remoteVideo.nativeElement.srcObject.getVideoTracks()[0].onended = event => {
          console.log('track onended');
        };
        this.remoteVideo.nativeElement.onplaying = event => {
          console.log('playing');
          console.log(event);
          
        }
        this.remoteVideo.nativeElement.remote.ondisconnect = event => {
          console.log('remote ondisconnect event');
          console.log(event);
          
        }
        this.remoteVideo.nativeElement.onstalled = event => {
          console.log('stalled');
          console.log(event);
        }
        this.remoteVideo.nativeElement.onpause = event => {
          console.log('paused');
          console.log(event);
        }
        this.remoteVideo.nativeElement.onwaiting = event => {
          console.log('waiting');
          console.log(event);
        }
        this.remoteVideo.nativeElement.onabort = event => {
          console.log('abort');
          console.log(event);
        }
        this.remoteVideo.nativeElement.onerror = event => {
          console.log('error');
          console.log(event);
        }
        this.remoteVideo.nativeElement.onsuspend = event => {
          console.log('suspend');
          console.log(event);
        }
        this.remoteVideo.nativeElement.onended = event => {
          console.log('ended');
          console.log(event);
        }
        this.remoteVideo.nativeElement.onemptied = event => {
          console.log('emptied');
          console.log(event);
        }
      };
  
      if(this.localStreamSrc){
        this.addStream(this.localStreamSrc);
      }
    }

    addStream(streamSrc){
      this.pc.addStream(streamSrc);
    }
  
    sendMessage(senderId, data) {
      let msg = this.signallingChannel.push({ sender: senderId, message: data });
      msg.remove();
    }
  
    readMessage(data) {
      if (!data) return;
      try {
        var msg = JSON.parse(data.message);
        var sender = data.sender;
        if (sender != this.senderId) {
          if (msg.ice != undefined && this.pc != null) {
            this.pc.addIceCandidate(new RTCIceCandidate(msg.ice));
          } else if (msg.sdp.type == "offer") {
            this.pc.setRemoteDescription(new RTCSessionDescription(msg.sdp))
              .then(() => this.pc.createAnswer())
              .then(answer => this.pc.setLocalDescription(answer))
              .then(() => this.sendMessage(this.senderId, JSON.stringify({ sdp: this.pc.localDescription })));
          } else if (msg.sdp.type == "answer") {
            this.pc.setRemoteDescription(new RTCSessionDescription(msg.sdp));
          }
        }
      } catch (error) {
        console.log(error);
      }
    }
  
    createOffer() {
      try {
        this.pc.createOffer()
          .then(offer => this.pc.setLocalDescription(offer))
          .then(() => {
            this.sendMessage(this.senderId, JSON.stringify({ sdp: this.pc.localDescription }));
          });
      } catch (error) {
        console.log(error);
      }
    }
  
    hangup() {
      this.signallingChannelSubscription.unsubscribe();
      this.signallingChannel.remove();
      this.pc.close();
    }
  
    guid() {
      return (this.s4() + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + this.s4() + this.s4());
    }
    s4() {
      return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

}
