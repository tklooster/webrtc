import { Component, OnInit, OnDestroy, ViewChild, ViewContainerRef, ComponentRef, ComponentFactory, ComponentFactoryResolver } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { CaptureScreenService } from 'src/app/services/capture-screen.service';
import { Observable, Subscription } from 'rxjs';

import { PresenceService } from '../services/presence.service';

import { PeerConnectionComponent } from '../peer-connection/peer-connection.component';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.scss']
})
export class ChannelComponent implements OnInit, OnDestroy {
  capturing: boolean = false;
  localStream: any = null;
  availableStreamsRef: AngularFireList<any>; 
  availableStreams: Observable<any[]>;
  availableStreamsEntry: any;
  shareRequestsRef: AngularFireList<any>; 
  shareRequestsSubscribtion: Subscription;

  peerConnections: any[] = new Array();

  fullHdConstraints = {
    video: {width: {exact: 1920}, height: {exact: 1080}}
  };

  @ViewChild("peerConnections", {read: ViewContainerRef}) container;
  @ViewChild("me") me: any;

  constructor(
    private db: AngularFireDatabase,
    private screenService: CaptureScreenService,
    private presenceService: PresenceService,
    private resolver: ComponentFactoryResolver
  ) { 
    
    this.availableStreamsRef = this.db.list('available-streams');
    this.availableStreams = this.availableStreamsRef.valueChanges();
    this.shareRequestsRef = this.db.list('requests/' + this.presenceService.sessionId);
    this.shareRequestsSubscribtion = this.shareRequestsRef.stateChanges(['child_added'])
    .subscribe(action => {
      console.log(action);
      if(action.payload.val()){
        this.createPeerConnection(action.payload.val().sessionId, action.payload.val().senderId, true);
      }
    });
  }

  ngOnInit() {
    
  }

  ngOnDestroy() {
    if(this.availableStreamsEntry){
      this.availableStreamsEntry.remove();
    }
    this.shareRequestsSubscribtion.unsubscribe();
    let tracks = this.localStream.getTracks();
    for (let i = 0; i < tracks.length; i++) {
      tracks[i].stop();
    }
  }

  startCapture() {
    this.screenService.capture()
    .then(stream => (this.me.nativeElement.srcObject = stream))
    .then(stream => {
      this.capturing = true;
      this.localStream = stream;
      if(this.presenceService.userId){
        this.availableStreamsRef.push({
          clientId: this.presenceService.sessionId,
          userId: this.presenceService.userId,
          displayName: this.presenceService.displayName
        }).then( data => {
          this.availableStreamsEntry = data;
          this.db.object(`available-streams/${data.key}`).query.ref.onDisconnect().remove();
        });
      }
      this.localStream.getVideoTracks()[0].onended = event => {
        this.capturing = false;
        if(this.availableStreamsEntry){
          this.availableStreamsEntry.remove();
        }
        this.destroyOutboundPeerConnections();
      };
    })
  }

  createPeerConnection(sessionId: string, remoteClientId: string, sendOffer: boolean = false) {
    const factory: ComponentFactory<PeerConnectionComponent> = this.resolver.resolveComponentFactory(PeerConnectionComponent);
    const componentRef = this.container.createComponent(factory);
    (<PeerConnectionComponent>componentRef.instance).peerSessionId = sessionId;
    (<PeerConnectionComponent>componentRef.instance).remoteClientId = remoteClientId;
    if(sendOffer){
      (<PeerConnectionComponent>componentRef.instance).localStreamSrc = this.localStream;
      (<PeerConnectionComponent>componentRef.instance).sendOffer = sendOffer;
    }
    let connectionStateChangeSubscription = (<PeerConnectionComponent>componentRef.instance).connectionStateChange.subscribe( state => {
      console.log(state);
      if(state == 'disconnected'){
        this.destroyPeerConnection(componentRef);
        connectionStateChangeSubscription.unsubscribe();
      }
    });
    this.peerConnections.push(componentRef);
  }

  logPeerConnections(){
    console.log(this.peerConnections);
  }

  destroyPeerConnection(destroyRef){
    this.peerConnections.forEach( (ref, index, object) => {
      if(ref == destroyRef){
        object.splice(index, 1);
      }
    });
    destroyRef.destroy();
  }

  destroyPeerConnections(){
    this.peerConnections.forEach( (ref, index, object) => {
      ref.destroy();
      object.splice(index, 1);
    });
  }

  destroyOutboundPeerConnections(){
    for(let ref of this.peerConnections ) {
      if(ref.instance.sendOffer){
        this.destroyPeerConnection(ref);
      }
    }
  }

  checkIfStreamIsActive(checkId: string) {
    console.log('comparing to ' + checkId)
    for(let connection of this.peerConnections ) {
      console.log(connection.instance.remoteClientId);
      if(connection.instance.remoteClientId == checkId && !connection.instance.sendOffer){
        console.log('found match');
        return true;
      }
    }
    return false;
  }

  countActiveInboundStreams() :number {
    let n = 0;
    for(let connection of this.peerConnections ) {
      if(!connection.instance.sendOffer){
        n = n + 1;
      }
    }
    return n;
  }

  createShareRequest(remoteClientId: string){
    if(remoteClientId){
      console.log('check func' + !this.checkIfStreamIsActive(remoteClientId))
      if(!this.checkIfStreamIsActive(remoteClientId)){
        let sessionId = 'peersession-' + this.guid();
        let request = this.db.list('requests/' + remoteClientId).push({
          sessionId: sessionId,
          senderId: this.presenceService.sessionId
        });
        request.remove();
        this.createPeerConnection(sessionId, remoteClientId);
      }
    }
  }

  guid() {
    return (this.s4() + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + this.s4() + this.s4());
  }
  
  s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }

}