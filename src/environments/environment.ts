// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAguH7b4fA7v0U8i_LymGBXbW_rjY6yTzA",
    authDomain: "webrtc-267216.firebaseapp.com",
    databaseURL: "https://webrtc-267216.firebaseio.com",
    projectId: "webrtc-267216",
    storageBucket: "webrtc-267216.appspot.com",
    messagingSenderId: "11845976254",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
